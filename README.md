JastAdd Logotype
================

The JastAdd logotype may only be used to represent the JastAdd meta-compiler
system. [http://jastadd.org](http://jastadd.org)

The JastAdd logotype can be used freely in presentation slides or similar
materials to refer to JastAdd.

The logo is Copyright (c) 2011 by The JastAdd Team.

The typeface used in the JastAdd logo is [Pusselbit by Jesper
&Ouml;qvist](http://llbit.se/?page_id=996). The typeface is licensed under the
[Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/).
